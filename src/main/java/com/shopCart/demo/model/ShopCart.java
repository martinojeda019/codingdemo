package com.shopCart.demo.model;

import java.io.Serializable;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;

@Entity
@Table(name = "s_cart")
public class ShopCart implements Serializable {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;

	@Column(name = "cart_item_no", length = 55)
	private String cartItemNo;

	@Column(name = "cart_total_count", length = 4)
	private int cartTotalCount;

	@Column(name = "cart_total_price", length = 9)
	private String cartTotalPrice;

	@OneToOne
	@JoinColumn(name = "cart_item_no", updatable = false, insertable = false)
	private ShopItem listOfItem;

	public ShopCart() {
		super();
	}

	public ShopCart(int id, int cartTotalCount, String cartTotalPrice) {
		super();
		this.id = id;
		this.cartTotalCount = cartTotalCount;
		this.cartTotalPrice = cartTotalPrice;
	}

	/**
	 * @return the id
	 */
	public int getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(int id) {
		this.id = id;
	}

	/**
	 * @return the cartItemNo
	 */
	public String getCartItemNo() {
		return cartItemNo;
	}

	/**
	 * @param cartItemNo the cartItemNo to set
	 */
	public void setCartItemNo(String cartItemNo) {
		this.cartItemNo = cartItemNo;
	}

	/**
	 * @return the cartTotalCount
	 */
	public int getCartTotalCount() {
		return cartTotalCount;
	}

	/**
	 * @param cartTotalCount the cartTotalCount to set
	 */
	public void setCartTotalCount(int cartTotalCount) {
		this.cartTotalCount = cartTotalCount;
	}

	/**
	 * @return the cartTotalPrice
	 */
	public String getCartTotalPrice() {
		return cartTotalPrice;
	}

	/**
	 * @param cartTotalPrice the cartTotalPrice to set
	 */
	public void setCartTotalPrice(String cartTotalPrice) {
		this.cartTotalPrice = cartTotalPrice;
	}

	/**
	 * @return the listOfItem
	 */
	public ShopItem getListOfItem() {
		return listOfItem;
	}

	/**
	 * @param listOfItem the listOfItem to set
	 */
	public void setListOfItem(ShopItem listOfItem) {
		this.listOfItem = listOfItem;
	}

	@Override
	public String toString() {
		return "ShopCart [id=" + id + ", cartTotalCount=" + cartTotalCount + ", cartTotalPrice=" + cartTotalPrice
				+ ", listOfItem=" + listOfItem + "]";
	}

}
