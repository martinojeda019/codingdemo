package com.shopCart.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopCart.demo.model.ShopItem;

@Repository
public interface ShopItemRepository extends JpaRepository<ShopItem, Integer> {

	public ShopItem findByItemNo(String itemNo);

	public int deleteByItemNo(String itemNo);
}
