package com.shopCart.demo.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import com.shopCart.demo.model.ShopCart;

@Repository
public interface ShopCartRepository extends JpaRepository<ShopCart, Integer> {

	public ShopCart findByCartItemNo(String cartItemNo);

	public int deleteByCartItemNo(String cartItemNo);
}
