package com.shopCart.demo.serviceImpl;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopCart.demo.Utils.InputValidation;
import com.shopCart.demo.model.ShopItem;
import com.shopCart.demo.repository.ShopItemRepository;
import com.shopCart.demo.service.ShopItemService;

@Service
@Transactional
public class ShopItemServiceImpl implements ShopItemService {

	@Autowired
	private ShopItemRepository shopItemRepository;

	@Override
	public boolean createItem(ShopItem shopItem) {
		if (!isvalidForm(shopItem)) {
			return false;
		}
		shopItemRepository.save(shopItem);
		return true;
	}

	@Override
	public int removeItem(String itemNo) {
		if (InputValidation.isNotNullOrEmpty(itemNo)) {
			return shopItemRepository.deleteByItemNo(itemNo);
		}
		return 0;
	}

	@Override
	public boolean updateItem(ShopItem shopItem) {
		if (!isvalidForm(shopItem)) {
			return false;
		}
		shopItemRepository.save(shopItem);
		return true;
	}
	
	@Override
	public List<ShopItem> getListOfItem() {
		return shopItemRepository.findAll();
	}

	private boolean isvalidForm(ShopItem shopItem) {

		if (!InputValidation.isNotNullOrEmpty(shopItem.getItemNo())) {
			return false;
		}
		if (!InputValidation.isNotNullOrEmpty(shopItem.getItemName())) {
			return false;
		}
		if (!InputValidation.isNotNullOrEmpty(shopItem.getItemPrice())) {
			return false;
		}
		if (!InputValidation.isNotNullOrEmpty(String.valueOf(shopItem.getItemQty()))) {
			return false;
		}
		return true;
	}


}
