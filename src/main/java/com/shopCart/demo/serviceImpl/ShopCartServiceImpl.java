package com.shopCart.demo.serviceImpl;

import java.util.List;
import java.util.stream.Collectors;

import javax.transaction.Transactional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.shopCart.demo.Utils.InputValidation;
import com.shopCart.demo.model.ShopCart;
import com.shopCart.demo.model.ShopItem;
import com.shopCart.demo.repository.ShopCartRepository;
import com.shopCart.demo.repository.ShopItemRepository;
import com.shopCart.demo.service.ShopCartService;

@Service
@Transactional
public class ShopCartServiceImpl implements ShopCartService {

	@Autowired
	private ShopItemRepository shopItemRepository;

	@Autowired
	private ShopCartRepository shopCartRepository;

	@Override
	public boolean addItemToCart(String shopItemNo) {
		if (!InputValidation.isNotNullOrEmpty(shopItemNo)) {
			return false;
		}
		final ShopItem shopItem = shopItemRepository.findByItemNo(shopItemNo);
		final ShopCart shopCart = new ShopCart();

		if (shopCartRepository.findByCartItemNo(shopItemNo) != null) {
			if (addQty(shopItemNo) > 0) {
				return true;
			}
			return false;
		}
		shopCart.setCartItemNo(shopItemNo);
		shopCart.setCartTotalCount(1);
		shopCart.setCartTotalPrice(shopItem.getItemPrice());
		// shopCart.setListOfItem(shopItem);
		shopCartRepository.save(shopCart);
		return true;
	}

	@Override
	public int addQty(String itemNo) {
		if (!InputValidation.isNotNullOrEmpty(itemNo)) {
			return 0;
		}
		final ShopCart shopCart = shopCartRepository.findByCartItemNo(itemNo);
		final ShopItem shopItem = shopItemRepository.findByItemNo(itemNo);

		shopCart.setCartTotalCount(shopCart.getCartTotalCount() + 1);
		shopCart.setCartTotalPrice(String
				.valueOf(Integer.parseInt(shopCart.getCartTotalPrice()) + Integer.parseInt(shopItem.getItemPrice())));
		shopCartRepository.save(shopCart);
		return 1;
	}

	@Override
	public int removeQty(String itemNo) {
		if (!InputValidation.isNotNullOrEmpty(itemNo)) {
			return 0;
		}
		final ShopCart shopCart = shopCartRepository.findByCartItemNo(itemNo);
		final ShopItem shopItem = shopItemRepository.findByItemNo(itemNo);

		if (shopCart.getCartTotalCount() <= 1) {
			shopCartRepository.delete(shopCart);
			return 1;
		}
		shopCart.setCartTotalCount(shopCart.getCartTotalCount() - 1);
		shopCart.setCartTotalPrice(String
				.valueOf(Integer.parseInt(shopCart.getCartTotalPrice()) - Integer.parseInt(shopItem.getItemPrice())));
		shopCartRepository.save(shopCart);
		return 1;
	}

	@Override
	public String getTotalPrice() {
		List<ShopCart> listOfShopCart = shopCartRepository.findAll();
		Integer totalPriceOfItem = listOfShopCart.stream().map(price -> Integer.parseInt(price.getCartTotalPrice()))
				.collect(Collectors.summingInt(Integer::intValue));
		return String.valueOf(totalPriceOfItem);
	}

	@Override
	public List<ShopCart> getListOfCartItem() {
		return shopCartRepository.findAll();
	}
}
