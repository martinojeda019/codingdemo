package com.shopCart.demo.service;

import java.util.List;

import com.shopCart.demo.model.ShopCart;

public interface ShopCartService {

	public boolean addItemToCart(String shopItemNo);

	public int addQty(String itemNo);

	public int removeQty(String itemNo);
	
	public String getTotalPrice();
	
	public List<ShopCart> getListOfCartItem();
}
