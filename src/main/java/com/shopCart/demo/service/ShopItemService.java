package com.shopCart.demo.service;

import java.util.List;

import com.shopCart.demo.model.ShopItem;

public interface ShopItemService {

	public boolean createItem(ShopItem shopItem);

	public int removeItem(String itemNo);

	public boolean updateItem(ShopItem shopItem);

	public List<ShopItem> getListOfItem();
}
