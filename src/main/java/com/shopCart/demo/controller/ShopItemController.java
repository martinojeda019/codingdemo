package com.shopCart.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.shopCart.demo.model.ShopItem;
import com.shopCart.demo.serviceImpl.ShopItemServiceImpl;

@Controller
public class ShopItemController {

	@Autowired
	private ShopItemServiceImpl shopItemServiceImpl;

	@GetMapping(value = { "/", "/items" })
	public String index(Model model) {
		model.addAttribute("items", this.shopItemServiceImpl.getListOfItem());
		return "items";
	}

	@PostMapping(value = "/addItem")
	public String addItem(RedirectAttributes redirectAttributes, @ModelAttribute ShopItem shopItem) {
		if (!this.shopItemServiceImpl.createItem(shopItem)) {
			redirectAttributes.addFlashAttribute("error", "Shop Item Unsuccessfully Added!");
		} else {
			redirectAttributes.addFlashAttribute("msge", "Shop Item Successfully Added!");
		}
		return "redirect:/items";
	}

	@PostMapping(value = "/updateItem")
	public String updateItem(@RequestBody ShopItem shopItem) {
		return "redirect:/items";
	}

	@GetMapping(value = "/removeItem/{itemNo}")
	public String removeItem(@PathVariable String itemNo) {
		return "redirect:/items";
	}

}
