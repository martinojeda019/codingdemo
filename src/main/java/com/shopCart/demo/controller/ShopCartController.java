package com.shopCart.demo.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.shopCart.demo.serviceImpl.ShopCartServiceImpl;

@Controller
public class ShopCartController {

	@Autowired
	private ShopCartServiceImpl shopCartServiceImpl;

	@GetMapping(value = "/cart")
	public String listOfCartItem(Model model) {
		model.addAttribute("cart", this.shopCartServiceImpl.getListOfCartItem());
		model.addAttribute("totalPrice", this.shopCartServiceImpl.getTotalPrice());
		return "cart";
	}

	@GetMapping(value = "/addItemToCart/{shopItemNo}")
	public String addItemToCart(RedirectAttributes redirectAttributes, @PathVariable String shopItemNo) {
		if (!this.shopCartServiceImpl.addItemToCart(shopItemNo)) {
			redirectAttributes.addFlashAttribute("error", "Shop Item Unsuccessfully Added to Cart!");
		} else {
			redirectAttributes.addFlashAttribute("msge", "Shop Item Successfully Added to Cart");
		}
		return "redirect:/items";
	}

	@GetMapping(value = "/addQty/{shopItemNo}")
	public String addQty(RedirectAttributes redirectAttributes, @PathVariable String shopItemNo) {
		if (this.shopCartServiceImpl.addQty(shopItemNo) <= 0) {
			redirectAttributes.addFlashAttribute("error", "Shop Item Unsuccessfully Added!");
		} else {
			redirectAttributes.addFlashAttribute("msge", "Shop Item Successfully Added");
		}
		return "redirect:/cart";
	}

	@GetMapping(value = "/removeQty/{shopItemNo}")
	public String removeQty(RedirectAttributes redirectAttributes, @PathVariable String shopItemNo) {
		if (this.shopCartServiceImpl.removeQty(shopItemNo) <= 0) {
			redirectAttributes.addFlashAttribute("error", "Shop Item Unsuccessfully Added!");
		} else {
			redirectAttributes.addFlashAttribute("msge", "Shop Item Successfully Added");
		}
		return "redirect:/cart";
	}
}
